const { RpcWebSocketClient } = require('rpc-websocket-client');
 
(async () => {
 	
 	try{
	    const rpc = new RpcWebSocketClient();
	 
	    await rpc.connect('wss://qap.dss.gov.au/app/3e864bb9-8c49-493a-ab3f-4e29cb204e2e?reloadUri=https://www.ndiscommission.gov.au/resources/ndis-provider-register/search');
	    // connection established
	
	    // err is typeof RpcError (code: number, message: string, data?: any)
	    await rpc.call('SearchResults',{params:[{qSearchFields:["ABN"],qContext:"Cleared"},["24109545968"],{qOffset:0,qCount:1}]});
	 	
	 	// "{"delta":true,"handle":1,"method":"SearchResults","params":[{"qSearchFields":["ABN"],"qContext":"Cleared"},["24109545968"],{"qOffset":0,"qCount":1}],"id":18,"jsonrpc":"2.0"}"
	 	
	}catch(error){
		console.log(error)
	}
})();